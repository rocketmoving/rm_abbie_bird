# Import libraries
from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# import libraries for Bit.ly
from bitlyshortener import Shortener
from my_secrets import bitly_secret

# Import libraries
from datetime import date
from time import sleep
from my_secrets import gscopes, gspreadsheetid


# If modifying these scopes, delete the file token.pickle.
SCOPES = gscopes

# The ID and range of a sample spreadsheet.
"""
This spread sheet is mapped as follows:
Column A = Title
Column B = URL
Column C = Date Harvested
Column D = Date Posted
Column E = Posted
"""

SPREADSHEET_ID = gspreadsheetid
RANGE_NAME = 'News!A2:F'
ARTICLE_RANGE = 'News!A2:A'


def build_service():
    """Build the service with Google Sheets."""
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'twitter_sheets_secret.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    # Build the service
    service = build('sheets', 'v4', credentials=creds)
    return service


def get_current_titles(service):
    """Get's current list of article titles from Google Sheets."""
    # Call the Sheets API
    sheet = service.spreadsheets()
    # Get the result
    article_titles = sheet.values().get(spreadsheetId=SPREADSHEET_ID, range=ARTICLE_RANGE).execute()
    # Store the values
    values = article_titles.get('values', [])
    # If no data found
    if not values:
        print('No data found.')
    # If data exists
    else:
        # Create empty list for titles
        titles = []
        # Iterate through each row
        for row in values:
            title = row[0]
            print(f'Found a title: {title}')
            titles.append(title)
    # TODO print debug -> input('Holding after result')
    return titles


def add_new_titles(service, new_data):
    """Adds new articles to Google Sheets."""
    # Call the Sheets API
    sheet = service.spreadsheets()
    # Get the result
    current_articles = sheet.values().get(spreadsheetId=SPREADSHEET_ID, range=ARTICLE_RANGE).execute()
    # Store the values
    values = current_articles.get('values', [])
    # If no data found
    if not values:
        print('No data found.')
    # If data exists
    else:
        # Establish a counter
        append_at_row = 2
        # Iterate through each row
        for row in values:
            print(f'Found a title: {row}')
            append_at_row += 1
    print(f'Appending data at this row: {str(append_at_row)}')        
    sleep(3)

    # Create an empty dictionary
    sheets_json = {}
    # Create an empty list
    sheets_input = []
    # Iterate through each article in dictionary
    for unique_article in new_data:
        # Create an empty list
        this_article = []
        # Add the title to the list
        this_article.append(unique_article['title'])
        # Add the url to the list
        this_article.append(unique_article['url'])
        # Add the current date
        today = date.today()
        # mm/dd/y
        d3 = today.strftime("%m/%d/%y")
        this_article.append(d3)
        # Add this article to the sheets input list
        sheets_input.append(this_article)
    
    # Fill the JSON object
    sheets_json.update({'values': sheets_input})
    print(f'JSON: {sheets_json}')

    # Set the range
    APPEND_TO_RANGE = f'News!A{str(append_at_row)}:D'
    # Define the input values
    input_values = sheets_json
    # Append the data
    appended_to_sheet = sheet.values().append(spreadsheetId=SPREADSHEET_ID, range=APPEND_TO_RANGE, 
            valueInputOption='RAW', body=input_values).execute()


def get_article_titles(service):
    """Function to get the article titles from Hacker News."""
    # Call the Sheets API
    sheet = service.spreadsheets()
    # Get the result
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                range=RANGE_NAME).execute()
    values = result.get('values', [])

    # Create an empty list for all titles
    all_article_titles = []
    # If no data is returned
    if not values:
        print('No data found.')
    #If data exists
    else:
        # Iterate through each row
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            existing_title = (row[0])
            # If title data exists
            if existing_title:
                # TODO debug TODO print(existing_title)
                # Add article title to all titles list
                all_article_titles.append(existing_title)
    #print(f'All articles {all_article_titles}')
    return all_article_titles


def add_to_sheet(data_to_add, service):
    """Add the new list of articles to Google Sheets."""
    # Call the Sheets API
    sheet = service.spreadsheets()


    def get_sheets_last_row(sheet):
        """Get the current length of the Google Sheet.""" 
        # Use column A only
        RANGE_COLUMN_A = 'News!A2:A'
        # Get the result
        column_a = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                    range=RANGE_COLUMN_A).execute()
        # Get the length of data in Column A
        column_a_length = len(column_a)
        print(f'Length Column A {column_a_length}')

        return column_a_length


    def format_input_data(format_me):
        """Format the dictionary of articles into a JSON object."""
        print(f'Format me: {format_me}')
        # Create an empty dictionary
        sheets_json = {}
        # Create an empty list
        sheets_input = []
        # Iterate through each article dictionary
        for unique_article in format_me:
            # Create an empty list
            sheets_article = []
            # Add title to the list
            title = unique_article['title']
            #print(f'title {title}')
            sheets_article.append(title)

            # Add url to the list
            sheets_article.append(unique_article['url'])
            # Add date to the list
            today = date.today()
            # mm/dd/y
            d3 = today.strftime("%m/%d/%y")
            sheets_article.append(d3)
            # Add article list to sheets input list
            sheets_input.append(sheets_article)

        # Fill the JSON object
        sheets_json.update({'values': sheets_input})
        print(f'JSON: {sheets_json}')

        return sheets_json

    def add_data(formatted_data, starting_row, sheet):
        """Send starting_row to sheets then append data_to_add."""

        # Correct the starting row location
        #starting_row += 1
        # Define the range
        APPEND_TO_RANGE = f'News!A{str(starting_row)}:D'
        # Define the input values
        input_values = formatted_data
        # Append the data
        appended_to_sheet = sheet.values().append(
            spreadsheetId=SPREADSHEET_ID, range=APPEND_TO_RANGE, 
            valueInputOption='RAW', body=input_values).execute()

    # Calculate the last row
    column_a_length = get_sheets_last_row(sheet)

    # Format the input data
    formatted_data = format_input_data(format_me=data_to_add)
    print(f'Found formatted data {formatted_data}')


    # Add the data to Google Sheets
    add_data(formatted_data=formatted_data, starting_row=column_a_length, sheet=sheet)


def get_article_for_tweet(service):
    """Get the last article in Sheets that has not been posted yet."""

    def bitly_me(link):
        """Shorten link"""
        # shorten the link using Bit.ly
        tokens_pool = [bitly_secret]
        shortener = Shortener(tokens=tokens_pool, max_cache_size=8192)
        bit_urls = [link]
        bitly_link = shortener.shorten_urls(bit_urls)
        bitly_link = bitly_link[0]
        return bitly_link

    # Call the Sheets API
    sheet = service.spreadsheets()

    # Get the Sheets API result
    sheet_for_tweet = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                range=RANGE_NAME).execute()
    data_up_to_row = sheet_for_tweet.get('values', [])
    # find the last row that was posted and take the following row's data
    row_num = 2
    # Create an empty list for the article to tweet
    article_to_tweet = []
    for each_row in data_up_to_row:
        if 'Yes' in each_row[4]:
            row_num += 1
        elif 'No' in each_row[4]:
            # Get the title
            tw_title = each_row[0]
            # Get the article url
            article_link = each_row[1]
            tw_link = bitly_me(article_link)
            # Add to article to tweet
            article_to_tweet.append(tw_title)         
            article_to_tweet.append(tw_link)         
            # End loop on first unposted row
            break

    return article_to_tweet, row_num


def update_sheets_posted(row_num, service):
    """Update the article as 'Posted = Yes' on Google Sheets."""
    # Find today's date
    today = date.today()
    # mm/dd/y
    posted_on = today.strftime("%m/%d/%y")

    # Call the Sheets API
    sheet = service.spreadsheets()
    # Define the range
    UPDATE_RANGE = 'News!D'+str(row_num)
    # Values to insert
    tweet_values = [
        [
            posted_on,
            'Yes',
        ],
    ]

    Body = {
        'values' : tweet_values,
        'majorDimension' : 'ROWS',   
    }   
    # Update Sheets via the API
    tweet_result = service.spreadsheets().values().update(
        spreadsheetId=SPREADSHEET_ID, range=UPDATE_RANGE, 
        valueInputOption='RAW', body=Body).execute()


