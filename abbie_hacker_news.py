# Script to extract news from the Hacker News API

# Import libraries
import requests
import json
from time import sleep
import os


def build_new_list():
    """ Create a list of new articles."""
    
    # Clear the terminal
    os.system('cls')

    # Define keywords
    keywords = ['hardware', 'regular expression', 'developing', 'android', 'tablet',
        'india', 'whatsapp', 'communications', 'firefox', 'securities', 'fraud',
        'co-founding', 'parse', 'google', 'recaptcha', 'pentagon', 'laser', 'developer', 
        'chrome', 'url', 'ad-blocker', 'implementation', 'queueing', 'theory', 'ads', 
        'websites', 'cisco', 'collaboration', 'codec', 'app', 'numpy', 'data',
        'startup', 'representation', 'apple', 'self-driving', 'startup', 'streaming', 
        'tech', 'idea', 'einstein', 'discoveries', 'discovery', 'smartphones',
        'smartphone', 'robots', 'robot', 'factory', 'jobs', 'facebook', 'salaries', 
        'a.i.', 'malware', 'iot', 'python', 'applications', 'ubuntu', '32-bit', 
        '64-bit', 'saas', 'software', 'email', 'hackers']

    # Removes any internally pointing links
    banned_urls = ['news.ycombinator.com']

    # Set the number of submissions to retrieve
    max_sub = 50

    # Make an api call and store the response
    hn_url = 'https://hacker-news.firebaseio.com/v0/topstories.json'
    r = requests.get(hn_url)
    print(f'Status code: {r.status_code}')
    print(f'Processing {max_sub} of the most recent top articles on Hacker News. Please wait.')

    # Get the json from the response
    submission_ids = r.json()
    # Create an empty list to hold approved submissions
    submission_dicts = []
    # Iterate through each submission
    for submission_id in submission_ids[:max_sub]:
        # Make a separate API call for each submission
        hn_url = f'https://hacker-news.firebaseio.com/v0/item/{submission_id}.json'
        r = requests.get(hn_url)
        # TODO print debug -> print(f'id: {submission_id}\tstatus: {r.status_code}') 
        # Get the json for this submission
        response_dict = r.json()
        # Set a flag
        add_article = False

        try:
            # Get the title and url
            title = response_dict['title']
            article_url = response_dict['url']
            # TODO print debug -> print(f'Extracted title: {title}\nExtracted url: {article_url}')
            # Check if any keywords are in the title
            for keyword in keywords:
                # If keyword is found, set flag to True
                if keyword in title.lower():
                    # TODO print debug -> print(f'Found keyword {keyword}')
                    add_article = True
                    break
            # Check if the url is a banned url
            for banned_url in banned_urls:
                # If banned url is in url, set flag to False
                if banned_url in article_url:
                    # TODO print debug -> print(f'Found banned url {banned_url}')
                    add_article = False

            # TODO print debug -> print(f'Adding article: {add_article}')

            # If flag is true, add data to dictionary and add dictionary to list
            if add_article == True:
                # Build a dictionary for each article
                submission_dict = {
                    'title': title,
                    'url': article_url,
                }
                submission_dicts.append(submission_dict)
        
        except:
            # If any errors, pass
            pass

    # Display all entries to be added
    hn_length = len(submission_dicts)
    for idx, submission_dict in enumerate(submission_dicts):
        print(f"# {str(int(idx+1))}: Title: {submission_dict['title']}")
        print(f"Link: {submission_dict['url']}\n")
    
    sleep(4)
    # Clear the terminal
    os.system('cls')
    # Print summary
    print(f'Created a list of {hn_length} articles.\n{submission_dicts}')
    sleep(4)
    return submission_dicts, hn_length
