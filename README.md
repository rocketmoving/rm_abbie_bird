## ABBIE BIRD 
**A bot for gathering news from the Hacker News API and interacting with Google Sheets, Twitter, and Telegram APIs.**

Written in Python 🐍

If you've enjoyed this content, give us a shoutout on Instagram: [@rocketmovingapp](https://www.instagram.com/rocketmovingapp) 

*View more content on our [Rocket Moving App development blog](https://rocketmoving.dev).*

---

## Upcoming Sprints

1. Refactor for Raspbian set up
2. Set cron job for abbie_update_news_content

---

## Notes

* Using Python 3.7.3
* You'll note several 'TODO' 's in the code. Enable/delete these print statements as you please.

---

## Requirements

1. Install a virtual terminal (Optional)
*Windows: 'python -m venv venv'*

2. Install the requests library
*Windows: 'pip install requests'*

3. Install the Google Client Library
**A Google Cloud Console account will be required to access Google's APIs**
*Windows: 'pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib'*

4. Install the python-telegram-bot library 
**A Telegram account and telegram bot will be required**
*Windows: 'pip install python-telegram-bot'*

5. Install the Tweepy library
**A Twitter account and Twitter Application will be required**
*Windows: 'pip install tweepy'*

---

## Scripts

**1. abbie_update_news_content.py**

* Extracts top articles from Hacker News (limit set to 50)
* Checks Google Sheets for existing articles
* Add new articles to Google Sheets
* Sends admin a telegram notification

*Windows: Run 'python abbie_update_news_content.py'*


**2. abbie_post_news.py**

* Extracts an article's details from Google Sheets
* Shortens the url with Bit.ly
* Tweets the article on Twitter
* Updates Google Sheets
* Sends admin a telegram notification

*Windows: Run 'python abbie_post_news.py'*

---