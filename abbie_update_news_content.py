# Import libraries from abbie_sheets_twitter
from __future__ import print_function
import json
import os.path
import pickle
import re
from abbie_sheets_twitter import add_to_sheet, build_service 
from abbie_sheets_twitter import get_article_titles, get_current_titles
from abbie_sheets_twitter import add_new_titles
from abbie_telegram import notify_admin
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from time import sleep

# Import libraries from abbie_hacker_news
import requests

# Import modules
from abbie_hacker_news import build_new_list
import os

# import libraries for Bit.ly
# TODO Not in use yet:
# from bitlyshortener import Shortener
# from my_secrets import bitly_secret


# Import Telegram credentials
from my_secrets import bitly_secret, chat_id, telegram_token

# Import libraries for Telegram
from telegram.ext import CommandHandler, Updater


# Run the bot
"""Flow:
1. Access Hacker News API. Build a list of news articles to post on Twitter.
2. Access Google Sheets API. Build a list of article titles already in Sheets.
3. For each article from Hacker News that is NOT in Sheets, add to Sheets.
# """

# Clear the terminal
os.system('cls')

# Run the function to build a list of articles and store the result
submission_dicts, hn_length = build_new_list()
# Print the result 
print(submission_dicts)

# Build a Google Sheets service
service = build_service()
# Get the current titles from Google sheets
current_titles = get_current_titles(service=service)

# Create an empty list for new articles
new_articles = []
for submission_dict in submission_dicts:
	if submission_dict['title'] not in current_titles:
		print(f"This is a new article: {submission_dict['title']}")
		new_articles.append(submission_dict)
    	# TODO print debug -> input(f'Holding at current titles\n\n{current_titles}')

# Clear the terminal
os.system('cls')
print(f'Adding a total of {len(new_articles)} articles.')
sleep(2)

# Set the admin notification
if len(new_articles) == 0:
    msg = f"""Hello team,\n There are no new articles to mine data from.\nThank you"""
else:
    msg = f"""Hello team,\n I have mined {len(new_articles)} new articles and added data to Google Sheets. I will post as content on Twitter.\nThank you"""	
# Advise administrators
notify_admin(admin_notify_token=telegram_token, admin_msg=msg, chat_id=chat_id)

msg = ''
for article_set in new_articles:
	msg = msg+article_set['title']+'\n'+article_set['url']+'\n\n'

msg = f"""Here are the articles:\n{msg}\nThank you"""	
# Advise administrators
notify_admin(admin_notify_token=telegram_token, admin_msg=msg, chat_id=chat_id)

# Add to Google Sheets
add_new_titles(service=service, new_data=new_articles)