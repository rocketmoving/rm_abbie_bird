# Import libraries for Telegram
from telegram.ext import Updater, CommandHandler
import requests
import re
import json 

# Import Telegram credentials
from my_secrets import telegram_token, chat_id

# Notify administrators
def notify_admin(admin_notify_token, admin_msg, chat_id):
    """Function to advise admin when ABBIE is working."""
    # Update admin via Telegram 
    TOKEN = admin_notify_token
    URL = "https://api.telegram.org/bot{}/".format(TOKEN)
    the_admin_msg = admin_msg
    # TODO print debug -> print(the_admin_msg)
   
   
    # Get the Telegram URL
    def get_telegram_url(url):
        """Get the formatted url."""
        response = requests.get(url)
        content = response.content.decode("utf8")
        # print(content) TODO print debug
        return content

   
    # Send Message
    def send_telegram_message(admin_msg, a_chat):
        """Send the message."""
        # Compile the Telegram send message url
        send_url = URL + "sendMessage?text={}&chat_id={}".format(admin_msg, a_chat)
        get_telegram_url(send_url)


    # Send the message
    send_telegram_message(admin_msg=the_admin_msg, a_chat=chat_id)
    
