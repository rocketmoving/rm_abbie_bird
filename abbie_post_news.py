# Import libraries from abbie_sheets_twitter
from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from my_secrets import gscopes, gspreadsheetid

# import libraries for Bit.ly
from bitlyshortener import Shortener
from my_secrets import bitly_secret

# Import libraries from abbie_hacker_news
import requests
import json

# Import libraries for Telegram
from telegram.ext import CommandHandler, Updater
import requests
import re
import json 

# Import Telegram credentials
from my_secrets import telegram_token, chat_id

# Import libraries for Twitter
import tweepy 
import time
from my_secrets import consumer_key, consumer_secret, access_token, access_secret
import random

# Import modules
from abbie_hacker_news import build_new_list
from abbie_sheets_twitter import build_service, get_article_titles
from abbie_sheets_twitter import add_to_sheet, get_article_for_tweet
from abbie_sheets_twitter import update_sheets_posted
from abbie_telegram import notify_admin
from random import choice
import os
from time import sleep

# Run the bot
"""Flow:
1. Access Google Sheets API. Get the details for the last article that has not been posted.
2. API with Twitter to tweet article.
3. Update Google Sheets 'Posted' from No to Yes.
"""


def post_tweet():
    """Post an article to Twitter."""

    # Create a list of greetings
    tw_greetings = [
        'Interesting read:\n',
        'I love to tweet tech content:\n',
        'Tweeting a article we think you\'d enjoy:\n',
        'Our \'tech news\' bot recommends this read:\n',
        'An interesting story on Hacker News:\n',
        'I love sharing tech related content:\n',
        'Tweeting some tech we think you\'d enjoy:\n',
        'Tweeting a article we think you\'d like:\n',
        'Tech news we think you\'d enjoy:\n',
        'A tech article we think you\'d like:\n',
        'We love tech articles!\n',
        'We think you\'d enjoy this read:\n',
        'Interested in technology?\n',
        'An article recommended by our bot:\n',
        'How has tech changed your life?\n',
        'A great article we read on Hacker News:\n',
        'Tweeting you a tech story:\n',
        'A read we think you\'d enjoy:\n',
        'A great tech related article:\n',
        'Tweeting an article we think you\'d like:\n',
        'Tweeting some tech think you\'d like:\n',
        'We enjoyed this read:\n',
        'I love tweet tech news:\n',
        'An interesting article about tech:\n',
        'Our \'tech news\' bot suggests this story:\n',
        'Tweeting an article we think you\'d enjoy:\n',
    ]
    # Randomly choose a greeting for Twitter 
    tw_greeting = choice(tw_greetings)

    # Create a list of hashtags
    hashtags_for_post = [
        '#tech', '#startup', '#robot', '#python'
    ]
    # Randomly choose a hashtag for Twitter 
    tw_hashtag = choice(hashtags_for_post)

    # Build a Google Sheets API service
    service = build_service()

    # Get the article to post
    article_to_tweet, row_num = get_article_for_tweet(service)

    # credentials to login to twitter api
    CONSUMER_KEY = consumer_key
    CONSUMER_SECRET = consumer_secret
    ACCESS_KEY = access_token
    ACCESS_SECRET = access_secret

    # Login to Twitter api
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
    api = tweepy.API(auth)

    try:
        # Tweet
        tw_title = article_to_tweet[0]
        tw_url = article_to_tweet[1]
        # Define tweet variations
        tweet_string = tw_greeting + tw_title + "\n" + tw_url
        tweet_string_w_keyword = tw_greeting + tw_title + "\n" + tw_url + "\n" + tw_hashtag
        # Count characters in tweet variations
        tw_str_count = 0
        for tw_str_i in tweet_string:
            tw_str_count += 1
        tw_str_k_count = 0
        for tw_str_k_i in tweet_string_w_keyword:
            tw_str_k_count += 1
        # Define tweet message
        if tw_str_k_count < 140:
            tweet_message = tweet_string_w_keyword
        else:
            tweet_message = tweet_string
        # Send the tweet
        api.update_status(tweet_message)

        # Advise admin when work is complete
        msg = f"""Hello team,\nI have tweeted on Twitter.\n\n\"{tw_greeting}\n{tw_title}\n{tw_url}\"\n\nThank you."""
        success_message = 'Success'

    except:
        # Advise admin there was a problem
        msg = f"""ERROR:\nHello team,\nI had a problem accessing Twitter's API.\n Thank you."""
        success_message = 'Fail'


    # Update Google Sheets
    update_sheets_posted(row_num, service)

    # Success message
    return success_message, msg


success_message, msg = post_tweet()

# Advise administrators
notify_admin(admin_notify_token=telegram_token, admin_msg=msg, chat_id=chat_id)
