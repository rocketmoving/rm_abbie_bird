# Telegram Credentials
telegram_token = '' # TODO insert Telegram token
chat_id = '' # TODO insert Telegram Chat ID

# Twitter Credentials
consumer_key = '' # TODO insert Twitter Consumer Key
consumer_secret = '' # TODO insert Twitter Consumer Secret
access_token = '' # TODO insert Twitter Access Token
access_secret = '' # TODO insert Twitter Access Secret

# Bitly Credentials
bitly_secret = '' # TODO insert Bitly Secret

# Google Sheets Credentials
# If modifying these scopes, delete the file token.pickle.
gscopes = ['https://www.googleapis.com/auth/spreadsheets']
# The ID and range of a sample spreadsheet.
gspreadsheetid = '' # TODO insert Google Spreadsheet Id
